<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CursoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [CursoController::class,'index'])->name('index');

Route::get('/agregar', [CursoController::class,'create'])->name('create');
Route::post('/agregar',[CursoController::class,'store'])->name('store');
Route::get('/editar/{id}', [CursoController::class,'edit'])->name('edit');
Route::post('/actualizar/{id}',[CursoController::class,'update'])->name('update');
Route::delete('/eliminar/{id}',[CursoController::class,'destroy'])->name('destroy');