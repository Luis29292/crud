@extends('plantilla')
@section('contenido')
<!--Contenido-->
<section class="content">
    <h1>Editar Curso</h1>
    <!--Formulario-->
    <form action="{{route('update',$curso->id)}}" method="POST">
        @csrf
        <!-- Email input -->
        <div data-mdb-input-init class="form-outline mb-4">
            <input type="text" id="nombre" class="form-control" value="{{$curso->nombre}}" name="nombre"/>
            <label class="form-label" for="nombre">Nombre</label>
            </div>
            <!-- Email input -->
        <div data-mdb-input-init class="form-outline mb-4">
            <input type="text" id="objetivo" class="form-control" value="{{$curso->objetivo}}" name="objetivo"/>
            <label class="form-label" for="objetivo">Objetivo</label>
            </div>
        <!-- 2 column grid layout with text inputs for the first and last names -->
        <div class="row mb-4">
          <div class="col">
            <div data-mdb-input-init class="form-outline">
              <input type="text" id="modalidad" name="modalidad" class="form-control" value="{{$curso->modalidad}}"/>
              <label class="form-label" for="modalidad">Modalidad</label>
            </div>
          </div>
          <div class="col">
            <div data-mdb-input-init class="form-outline">
              <input type="text" id="cupo" name="cupo" class="form-control" value="{{$curso->cupo}}"/>
              <label class="form-label" for="cupo">Cupo</label>
            </div>
          </div>
        </div>

        <!-- 2 column grid layout with text inputs for the first and last names -->
        <div class="row mb-4">
            <div class="col">
              <div data-mdb-input-init class="form-outline">
                <input type="text" id="periodo" name="periodo" class="form-control" value="{{$curso->periodo}}"/>
                <label class="form-label" for="periodo">Periodo</label>
              </div>
            </div>
            <div class="col">
              <div data-mdb-input-init class="form-outline">
                <input type="text" id="horario" name="horario" class="form-control" value="{{$curso->horario}}"/>
                <label class="form-label" for="horario">Horario</label>
              </div>
            </div>
          </div>

          <!-- 2 column grid layout with text inputs for the first and last names -->
        <div class="row mb-4">
            <div class="col">
              <div data-mdb-input-init class="form-outline">
                <input type="text" id="dias" name="dias" class="form-control" value="{{$curso->dias}}"/>
                <label class="form-label" for="dias">Dias</label>
              </div>
            </div>
            <div class="col">
              <div data-mdb-input-init class="form-outline">
                <input type="text" id="salon" name="salon" class="form-control" value="{{$curso->salon}}"/>
                <label class="form-label" for="salon">Salon</label>
              </div>
            </div>
          </div>
      
        <!-- Submit button -->
        <button data-mdb-ripple-init type="submit" class="btn btn-primary btn-block mb-4">Editar</button>
      
      </form>
</section>
@endsection